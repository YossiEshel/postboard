﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Postboard.Models
{
    [Table("Posts")]
    public class Post
    {
        public int PostId { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public string FileChosen { get; set; }
        public DateTime Created { get; set; }
    }
}