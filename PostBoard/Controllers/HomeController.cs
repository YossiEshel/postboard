﻿using Postboard.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Postboard.DAL;
using PagedList;

namespace PostBoard.Controllers
{
    public class HomeController : Controller
    {
        private PostContext db = new PostContext();
        // GET: Home
        public ViewResult Index(int? page)
        {
            try
            {
                var posts = from p in db.Posts
                            select p;
                posts = posts.OrderByDescending(x => x.Created);
                int pageSize = 11;
                int pageNumber = (page ?? 1);
                return View(posts.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception)
            {

                return View();
            }
        }
        public ActionResult AddPost(string name, string message, string fileChosen)
        {
            Post post = new Post() { Name = name, Message = message, FileChosen = fileChosen, Created = DateTime.Now };
            db.Posts.Add(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult FocusOnPostModal(int id)
        {
            Post post = db.Posts.Find(id);
            return View("FocusOnPostModal", post);
        }
    }
}